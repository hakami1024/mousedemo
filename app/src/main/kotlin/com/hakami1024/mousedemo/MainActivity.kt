package com.hakami1024.mousedemo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.hardware.usb.UsbAccessory
import android.hardware.usb.UsbManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.jakewharton.rxbinding.view.RxView
import org.jetbrains.anko.contentView
import org.jetbrains.anko.find
import org.jetbrains.anko.getStackTraceString
import rx.BackpressureOverflow
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import timber.log.Timber
import java.io.FileDescriptor
import java.io.FileOutputStream
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

/**
 * Created by hakami on 12/5/16.
 */

class MainActivity() : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view: View = mainContent()
        setContentView(view)

        setupTimber()

        Timber.d("onCreate")

        val pm = getPackageManager()
        val isUSBAccessory = pm.hasSystemFeature(PackageManager.FEATURE_USB_ACCESSORY)

        if (!isUSBAccessory) {
            makeToast("USB Accessory not supported")
            finish()
        }

        val manager: UsbManager = getSystemService(Context.USB_SERVICE) as UsbManager
        val accessory: UsbAccessory? = intent.getParcelableExtra<UsbAccessory>(UsbManager.EXTRA_ACCESSORY)

        if (accessory == null) {
            makeToast("Run from your PC as accessory!")
            finish()
            return
        }

        val file = manager.openAccessory(accessory).fileDescriptor
        if (file == null) {
            makeToast("Connection error, can not send data")
            finish()
            return
        }

        val touchObservable = RxView.touches(view)
                .sample(30, TimeUnit.MILLISECONDS)
                .onBackpressureBuffer(10, null, BackpressureOverflow.ON_OVERFLOW_DROP_OLDEST)
                .map { it.rawX.toInt() to it.rawY.toInt() }
                .share()

        val textView = view.find<TextView>(android.R.id.text1)

        touchObservable
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.toByteArray() }
                .subscribe {
                    val buffer = ByteBuffer.wrap(it)
                    textView.text = "${buffer.getInt()}, ${buffer.getInt()}"
                }

    setupConnection(touchObservable, file, accessory)
}

private fun setupTimber() {
    if (BuildConfig.DEBUG) {
        Timber.plant(Timber.DebugTree())
    }
}

private fun setupConnection(touchObservable: Observable<Pair<Int, Int>>, file: FileDescriptor, accessory: UsbAccessory) {
    val outputStream = FileOutputStream(file)

//        val point = Point()
//        windowManager.defaultDisplay.getSize(point)
//        outputStream.write((point.x to point.y).toByteArray())

    val detaching = PublishSubject.create<Unit>()
    detaching.subscribe {
        makeToast("Detaching")
        Timber.d("Detaching")

        outputStream.close()
        finish()
    }

    touchObservable
            .takeUntil(RxView.detaches(contentView!!))
            .takeUntil(detaching)
            .observeOn(Schedulers.io())
            .subscribe({ outputStream.write(it.toByteArray()) },
                    { makeToast(it.getStackTraceString()) })

    val detachReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.getAction()

            if (UsbManager.ACTION_USB_ACCESSORY_DETACHED == action) {
                if (intent.getParcelableExtra<UsbAccessory>(UsbManager.EXTRA_ACCESSORY) != null) {
                    if (intent.getParcelableExtra<UsbAccessory>(UsbManager.EXTRA_ACCESSORY) == accessory) {
                        detaching.onNext(Unit)
                        detaching.onCompleted()
                    }
                }
            }
        }
    }

    IntentFilter().apply {
        addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED)
        registerReceiver(detachReceiver, this)
    }
}
}

