package com.hakami1024.mousedemo

import android.content.Context
import android.hardware.SensorManager
import android.os.Looper
import android.support.annotation.MainThread
import android.widget.Toast
import org.jetbrains.anko.runOnUiThread
import java.nio.ByteBuffer

/**
 * Created by hakami on 12/6/16.
 */


fun FloatArray.toByteArray(): ByteArray {
    val buf = ByteBuffer.allocate(4 * size)

    forEach {
        buf.putFloat(it)
    }

    return buf.array()
}

fun isRelease(): Boolean =
        !BuildConfig.DEBUG

fun isMainThread(): Boolean =
        Looper.myLooper() == Looper.getMainLooper()

@MainThread
fun assertMainThread(): Unit =
        check(isMainThread() || isRelease()) { "wrong thread" }

fun Context.makeToast(str: String) {
    runOnUiThread {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show()
    }
}

fun statusString(status: Int) =
        when (status) {
            SensorManager.SENSOR_STATUS_UNRELIABLE -> "Unreliable"
            SensorManager.SENSOR_STATUS_NO_CONTACT -> "No contact"
            SensorManager.SENSOR_STATUS_ACCURACY_LOW -> "Accuracy low"
            SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM -> "Accuracy medium"
            SensorManager.SENSOR_STATUS_ACCURACY_HIGH -> "Accuracy high"
            else -> "Undefined accuracy"
        }

fun FloatArray.norm(): Float =
        Math.sqrt(map { (it * it).toDouble() }.sum()).toFloat()

fun Pair<Int, Int>.toByteArray(): ByteArray =
        ByteBuffer.allocate(4 * 2).apply {
            putInt(first)
            putInt(second)
        }.array()