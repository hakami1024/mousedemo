package com.hakami1024.mousedemo

import android.content.Context
import android.view.ViewGroup
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.textView

/**
 * Created by hakami on 12/5/16.
 */

fun Context.mainContent(): ViewGroup =
        linearLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)

            textView{
                id = android.R.id.text1
                text = "No coords"
            }


        }